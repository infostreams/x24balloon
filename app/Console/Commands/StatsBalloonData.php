<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;

class StatsBalloonData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'x24:stats ' .
            '{--noop : Iterate through the file and do nothing (speed comparison)} ' .
            '{--min : Display lowest temperature} ' .
            '{--max : Display highest temperature} ' .
            '{--mean : Display mean temperature} ' .
            '{--tally : Display observatory statistics} ' .
            '{--distance : Display distance travelled} ' .
            '{--lookahead=50 : Look ahead at most LOOKAHEAD entries to find out-of-order entries} ' .
            '{--normalize : Output normalized data}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Displays statistics about balloon data';

    /**
     * Display a table containing normalized measurements (in degrees Celsius and kilometers)
     *
     * @param \Generator $data
     */
    public function normalize(\Generator $data) {
        // can't use Symfony "Table" component because loads all rows
        // into memory before it generates the output
        $columns = [20, 20, 16, 11];

        $glue = ' | ';
        $width = array_sum($columns) + (count($columns) - 1) * strlen($glue) + 4;

        echo str_repeat("-", $width) . "\n";
        echo "| ". implode($glue, [
            str_pad('Timestamp', $columns[0]),
            str_pad('Location (km)', $columns[1]),
            str_pad('Temperature (°C)', $columns[2]),
            str_pad('Observatory', $columns[3])
        ]) . " |\n";
        echo str_repeat("-", $width) . "\n";
        foreach ($data as $line) {
            $data = Parser::parse($line);
            $time = date('Y-m-d\TH:i', $data['time']);
            $location = "(" .
                implode(', ', [
                    sprintf("%.2f", $data['location'][0]),
                    sprintf("%.2f", $data['location'][1])
                ]) .
            ")";
            $temperature = sprintf("%.2f", $data['temperature']);
            $observatory = $data['observatory'];

            echo "| " . implode($glue, [
                str_pad($time, $columns[0]),
                str_pad($location, $columns[1]),
                str_pad($temperature, $columns[2], " ", STR_PAD_LEFT),
                str_pad($observatory, $columns[3]),
            ]) . " |";
            echo "\n";
        }
        echo str_repeat("-", $width);
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = $this->readBalloonData();

        // if we need to display normalized data, do that and exit
        if ($this->option('normalize')) {
            return $this->normalize($data);
        }

        // create a new statistics object to calculate & hold our statistics data
        $stats = new Stats();

        // we cache this option instead of looking it up constantly
        $is_noop = $this->option('noop');

        // we create a buffer to find and out-of-order entries
        $lookahead = max(1, $this->option('lookahead'));
        $buffer = [];

        // create progress bar
        $bar = $this->output->createProgressBar();
        $step_size = 100000;

        // if the user has provided any options, we start processing
        if (count($this->options()) > 0) {
            foreach ($data as $nr=>$line) {
                if ($is_noop) {
                    // user provided the 'noop' option - so we do nothing
                    continue;
                }

                // parse the line
                $buffer[] = Parser::parse($line);

                if (count($buffer) == $lookahead) {
                    // if the buffer is full, update the statistics
                    // (and remove the processed line from the buffer)
                    $buffer = $stats->update($buffer);
                }

                if ($nr % $step_size === 0) {
                    // update the progress bar every now and then
                    $bar->advance($step_size);
                }
            }

            // process everything that's left in the buffer
            while (count($buffer) > 0) {
                $buffer = $stats->update($buffer);
            }
        }
        // remove the progress bar
        $bar->clear();

        // display the output
        $stats->display(
            $this->getOutput(),
            $this->option('min'),
            $this->option('max'),
            $this->option('mean'),
            $this->option('distance'),
            $this->option('tally')
        );
    }

    /**
     * @return \Generator
     */
    protected function readBalloonData() {
        $path = GenerateBalloonData::getBalloonDataPath(); // path would usually go in .env, or come from command line

        $f = fopen($path, 'r');
        while (!feof($f)) {
            $str = fgets($f);
            if (strlen(trim($str))>0) {
                yield $str;
            }
        }

        fclose($f);
    }
}

/**
 * Calculates statistics on a collection of weather balloon measurements
 *
 * @package App\Console\Commands
 */
class Stats {
    public $min = PHP_INT_MAX;
    public $max = PHP_INT_MIN;
    public $avg = 0;
    public $distance = 0;
    public $tally = [];

    protected $current_time = 0;
    protected $current_location = [null, null];
    protected $index = 0;

    public function __construct() {
        $countries = GenerateBalloonData::$countries;
        $this->tally = array_combine($countries, array_fill(0, count($countries), 0));
    }

    /**
     * Gets the entry that is closest (time-wise) to the entry that is currently
     * being processed. If two entries are equally close in time, the first one
     * in the buffer wins.
     *
     * @param array $buffer The buffer we're currently processing
     * @return array An array containing the next time entry, and the new buffer
     */
    protected function getNextEntry($buffer) {
        $closest = PHP_INT_MAX;
        $winner = 0;
        foreach ($buffer as $key=>$b) {
            if ($b["time"] - $this->current_time < $closest) {
                $closest = $b["time"] - $this->current_time;
                $winner = $key;
            }
        }

        $next = $buffer[$winner];
        unset($buffer[$winner]);

        return [$next, $buffer];
    }

    /**
     * Updates the statistics with the values from the 'next' entry
     *
     * @param array $buffer The current buffer
     * @return array The buffer that was provided, minus the entry that was processed
     */
    public function update($buffer) {
        // 95% plumbing, 5% actual calculation
        list($next, $buffer) = $this->getNextEntry($buffer);

        $temperature = $next['temperature'];
        $location = $next['location'];
        $observatory = $next['observatory'];

        if (is_numeric($temperature)) {
            $temperature = floatval($temperature);
            $this->min = min($temperature, $this->min);
            $this->max = max($temperature, $this->max);
            $this->avg = $this->avg + ($temperature - $this->avg) / (++$this->index);
        }
        if (!array_key_exists($observatory, $this->tally)) {
            $this->tally[$observatory] = 0;
        }
        $this->tally[$observatory]++;

        list($x, $y) = $location;
        if ($this->current_location[0] === null) {
            $this->distance = 0;
        } else {
            $x_diff = $this->current_location[0] - $x;
            $y_diff = $this->current_location[1] - $y;
            $this->distance += sqrt($x_diff * $x_diff + $y_diff * $y_diff);
        }
        $this->current_location = $location;

        return $buffer;
    }

    /**
     * Displays an overview table of the calculated statistics
     *
     * @param OutputInterface $output Determines where to output to
     * @param bool $show_min If 'true', includes the minimum temperature
     * @param bool $show_max If 'true', includes the maximum temperature
     * @param bool $show_mean If 'true', includes the average temperature
     * @param bool $show_distance If 'true', includes the distance traveled
     * @param bool $show_tally If 'true', includes a tally of contributed entries per country
     */
    public function display(
        OutputInterface $output,
        $show_min = false,
        $show_max = false,
        $show_mean = false,
        $show_distance = false,
        $show_tally = false)
    {
        $table = [];
        if ($show_min) {
            $table[] = ['min', sprintf("%.2f °C", $this->min)];
        }
        if ($show_max) {
            $table[] = ['max', sprintf("%.2f °C", $this->max)];
        }
        if ($show_mean) {
            $table[] = ['mean', sprintf("%.2f °C", $this->avg)];
        }
        if ($show_distance) {
            $table[] = ['distance', sprintf("%.2f km", $this->distance)];
        }
        if (count($table) > 0) {
            $this->table($output, ['Statistic', 'Value'], $table);
        }

        if ($show_tally) {
            $tally = [];
            foreach ($this->tally as $country=>$count) {
                $tally[$country] = [$country, $count];
            }
            $this->table($output, ['Country code', 'Number of recordings'], $tally);
        }
    }

    /**
     * Outputs a table
     *
     * @param OutputInterface $output
     * @param $headers
     * @param $rows
     */
    private function table(OutputInterface $output, $headers, $rows) {
        $table = new Table($output);

        $table->setHeaders((array) $headers)->setRows($rows)->render();
    }
}

/**
 * Parses a line from a balloon.csv file into a normalized set of values
 *
 * @package App\Console\Commands
 */
class Parser {

    /**
     * Returns a value in X, Y coordinates, expressed in kilometers
     *
     * @param $value
     * @param $observatory
     * @return array [x, y] coordinates
     */
    protected static function location($value, $observatory) {
        list($x, $y) = explode(",", $value.",");
        switch (strtoupper(trim($observatory))) {
            case "US":
                return [$x / 1.60934, $y / 1.60934];

            case "FR":
                return [$x / 1000, $y / 1000];

            case "AU":
            default:
                return [$x, $y];
        }
    }

    /**
     * Return the temperature, in degrees Celsius
     *
     * @param $value
     * @param $observatory
     * @return float
     */
    protected static function temperature($value, $observatory) {
        $value = floatval($value);
        switch (strtoupper(trim($observatory))) {
            case "US":
                return ($value - 32) * 5 / 9;

            case "AU":
                return $value;

            case "FR":
            default:
                return ($value - 273.15);
        }
    }

    /**
     * Parse one line from the input file and normalize the values
     *
     * @param $line
     * @return array
     */
    public static function parse($line) {
        // parse one line
        list(
            $timestamp,
            $location,
            $temperature,
            $observatory
        ) = str_getcsv($line . '||||', '|'); // append '||||' to ensure we always have 4 fields

        return [
            "time" => strtotime($timestamp),
            "location" => self::location($location, $observatory),
            "temperature" => self::temperature($temperature, $observatory),
            "observatory" => $observatory
        ];
    }
}
