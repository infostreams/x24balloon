<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Storage;
use Illuminate\Console\Command;

class GenerateBalloonData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'x24:generate {count=500M : How many lines of data should be generated}';

    protected $description = 'Generate fake balloon data';


    public static $countries = [
        "AD", "AE", "AF", "AG", "AI", "AL", "AM", "AN", "AO", "AQ", "AR", "AS", "AT", "AU", "AW",
        "AX", "AZ", "BA", "BB", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BL", "BM", "BN", "BO",
        "BQ", "BR", "BS", "BT", "BV", "BW", "BY", "BZ", "CA", "CC", "CD", "CF", "CG", "CH", "CI",
        "CK", "CL", "CM", "CN", "CO", "CR", "CS", "CU", "CV", "CW", "CX", "CY", "CZ", "DE", "DJ",
        "DK", "DM", "DO", "DZ", "EC", "EE", "EG", "EH", "ER", "ES", "ET", "FI", "FJ", "FK", "FM",
        "FO", "FR", "GA", "GB", "GD", "GE", "GF", "GG", "GH", "GI", "GL", "GM", "GN", "GP", "GQ",
        "GR", "GS", "GT", "GU", "GW", "GY", "HK", "HM", "HN", "HR", "HT", "HU", "ID", "IE", "IL",
        "IM", "IN", "IO", "IQ", "IR", "IS", "IT", "JE", "JM", "JO", "JP", "KE", "KG", "KH", "KI",
        "KM", "KN", "KP", "KR", "KW", "KY", "KZ", "LA", "LB", "LC", "LI", "LK", "LR", "LS", "LT",
        "LU", "LV", "LY", "MA", "MC", "MD", "ME", "MF", "MG", "MH", "MK", "ML", "MM", "MN", "MO",
        "MP", "MQ", "MR", "MS", "MT", "MU", "MV", "MW", "MX", "MY", "MZ", "NA", "NC", "NE", "NF",
        "NG", "NI", "NL", "NO", "NP", "NR", "NU", "NZ", "OM", "PA", "PE", "PF", "PG", "PH", "PK",
        "PL", "PM", "PN", "PR", "PS", "PT", "PW", "PY", "QA", "RE", "RO", "RS", "RU", "RW", "SA",
        "SB", "SC", "SD", "SE", "SG", "SH", "SI", "SJ", "SK", "SL", "SM", "SN", "SO", "SR", "SS",
        "ST", "SV", "SX", "SY", "SZ", "TC", "TD", "TF", "TG", "TH", "TJ", "TK", "TL", "TM", "TN",
        "TO", "TR", "TT", "TV", "TW", "TZ", "UA", "UG", "UM", "US", "UY", "UZ", "VA", "VC", "VE",
        "VG", "VI", "VN", "VU", "WF", "WS", "XK", "YE", "YT", "ZA", "ZM", "ZW"
    ];


    /**
     * @return mixed
     */
    public static function getBalloonDataPath() {
        return Storage::disk('public')->path('balloon-data.csv');
    }


    /**
     * Parses the 'count' argument into an integer.
     * Supported postfixes: 'k' for thousands, 'm' for millions, 'g' for billions.
     *
     * @return int
     */
    protected function parseCountArgument() {
        $count = strtolower(trim($this->argument('count')));

        switch ($count[-1]) {
            case 'k':
                $multiplier = 1000;
                break;
            case 'm':
                $multiplier = 1000000;
                break;
            case 'g':
                $multiplier = 1000000000;
                break;
            default:
                $multiplier = 1;
                break;
        }

        return intval(floatval($count) * $multiplier);
    }


    protected function timeAdjustment() {
        if (mt_rand(0, 100) > 80) {
            // we deliberately generate crappy data, so sometimes
            // we have non-increasing time between entries
            return - mt_rand(1, 30);
        }

        return + mt_rand(1, 300);
    }


    public function writeDataFile($path, $count) {
        $file = fopen($path, "w");

        if (!$file) {
            echo "Cannot write to file $path";
            exit(-1);
        }

        // determine starting time
        $time = mktime(0, 0, 0, 1, 1, mt_rand(1980, 2000));

        // disable time limit
        set_time_limit(0);

        // create progress bar
        $bar = $this->output->createProgressBar($count);

        $output = "";
        for ($i=0; $i<$count; $i++) {

            // pick a random country
            $code = self::$countries[array_rand(self::$countries)];

            // depending on which country we picked, generate a temperature and location
            switch ($code) {
                case "AU":
                    $temperature = $this->generateTemperatureInCelsius();
                    $location = $this->generateLocationInKilometers();
                    break;

                case "US":
                    $temperature = $this->generateTemperatureInFahrenheit();
                    $location = $this->generateLocationInMiles();
                    break;

                case "FR":
                    $temperature = $this->generateTemperatureInKelvin();
                    $location = $this->generateLocationInMeters();
                    break;

                default:
                    $temperature = $this->generateTemperatureInKelvin();
                    $location = $this->generateLocationInKilometers();
                    break;
            }

            // append result to output
            $output .= implode("|", [
                date("Y-m-d\TH:i", $time),
                $location,
                $temperature,
                $code
            ]) . "\n";

            // adjust the time
            $time += $this->timeAdjustment();

            if ($i % 10000 === 0) {
                // do a buffered write to the file
                fputs($file, $output);
                $output = "";

                // update progress bar
                $bar->advance(10000);
            }
        }

        // write whatever is left
        fputs($file, $output);

        $bar->finish();

        fclose($file);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->writeDataFile(self::getBalloonDataPath(), $this->parseCountArgument());
    }


    /**
     * Calculates an X, Y point on a sphere with a given radius,
     * and returns it as a string
     *
     * @param $radius
     * @return string
     */
    private function calculateXYLocation($radius) {
        // generate random latitude and longitude (in radians)
        $latitude = M_PI * mt_rand(0, 180) / 180;
        $longitude = M_PI * mt_rand(0, 180) / 180;

        // convert to Cartesian x, y coordinates
        $x = $radius * cos($latitude) * cos($longitude);
        $y = $radius * cos($latitude) * sin($longitude);

        return sprintf("%d,%d", $x, $y);
    }


    /**
     * Returns a field describing a location on earth, in kilometers
     *
     * @return string
     */
    protected function generateLocationInKilometers() {
        return $this->calculateXYLocation(6371 /* earth radius in km */);
    }

    /**
     * Returns a field describing a location on earth, in meters
     *
     * @return string
     */
    protected function generateLocationInMeters() {
        return $this->calculateXYLocation(6371000 /* earth radius in meters */);
    }

    /**
     * Returns a field describing a location on earth, in miles
     *
     * @return string
     */
    protected function generateLocationInMiles() {
        return $this->calculateXYLocation(3959 /* earth radius in miles */);
    }

    /**
     * Returns a temperature on Earth, in Kelvin.
     *
     * @return string
     */
    protected function generateTemperatureInKelvin() {
        return mt_rand(250, 300);
    }

    /**
     * Returns a temperature on Earth, in Celsius.
     *
     * @return string
     */
    protected function generateTemperatureInCelsius() {
        return $this->generateTemperatureInKelvin() - 273;
    }

    /**
     * Returns a temperature on Earth, in Fahrenheit.
     *
     * @return string
     */
    protected function generateTemperatureInFahrenheit() {
        return $this->generateTemperatureInKelvin() * 9 / 5 - 459;
    }
}
